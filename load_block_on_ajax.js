/**
 * @file
 */
 
(function ($) {

Drupal.loadBlockOnAjax = function () {
    $('.load-block-on-ajax-wrapper').click(function () {
      var load_on_ajax_div_id = $(this).find('div[id^=load-block-on-ajax-]').attr('id');
      var block_id = load_on_ajax_div_id.replace( 'load-block-on-ajax-', '' );
      block_id = block_id.replace( '-ajax-content', '' );
      $.ajax({
        url: Drupal.settings.basePath + "load-block-on-ajax/" + block_id + "?path=" + Drupal.settings.load_block_on_ajax_path,
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (data) {

        	var context = $('#load-block-on-ajax-'+ block_id + '-ajax-content');
//            Drupal.freezeHeight();
        	Drupal.detachBehaviors(context);
        	context.html( data['content'] );
        	if (data['ajaxblocks_settings']) {
        	  $.extend(true, Drupal.settings, data['ajaxblocks_settings']);
        	}
        	Drupal.attachBehaviors(context);
//            Drupal.unfreezeHeight();
        }
      });
    });
}
$(document).ready(function () {
	Drupal.loadBlockOnAjax();
});

})(jQuery);
